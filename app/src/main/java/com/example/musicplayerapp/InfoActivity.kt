package com.example.musicplayerapp

import android.media.MediaMetadataRetriever
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.widget.TextView

class InfoActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_info)

        //These 3 lines create a title and a back button for the song info activity
        val actionBar = supportActionBar
        actionBar!!.title = "Song Info"
        actionBar.setDisplayHomeAsUpEnabled(true)

        //Set variables to use listeners
        val songTitle: TextView = findViewById(R.id.SongTitle)
        val artist: TextView = findViewById(R.id.Artist)
        val year: TextView = findViewById(R.id.Year)


        val mediaPath: Uri = Uri.parse("android.resource://" + packageName + "/" + R.raw.music)
        val meta = MediaMetadataRetriever()
        meta.setDataSource(this, mediaPath)
        songTitle.text = meta.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE)
        artist.text = meta.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST)
        year.text = meta.extractMetadata(MediaMetadataRetriever.METADATA_KEY_YEAR)

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

}