package com.example.musicplayerapp

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.TaskStackBuilder
import android.content.Context
import android.content.Intent
import android.media.MediaMetadataRetriever
import android.media.MediaPlayer
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.example.musicplayerapp.InfoActivity as InfoActivity1

class MainActivity : AppCompatActivity() {
    //This var activates the media player
    private lateinit var mediaPlayer: MediaPlayer

    //The next 3 lines are used in the seekBar
    private lateinit var runnable: Runnable
    private var handler = Handler()
    private var songTime = 0

    //Notifications
    private val channelId = "Music Player App"
    private val channelName = "Music Player App Notifications"
    private val notificationId = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        createNotificationChannel()
        val notificationLayout = RemoteViews(packageName, R.layout.notification) // import custom notification

        //These next 2 val make the notification open the app when pressed
        val intentMain = Intent(this, MainActivity::class.java)
        val pendingIntent = TaskStackBuilder.create(this).run {
            addNextIntentWithParentStack(intentMain)
            getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT)
        }

        //I set up the notification
        val notification = NotificationCompat.Builder(this, channelId)
            .setStyle(NotificationCompat.DecoratedCustomViewStyle())
            .setSmallIcon(R.drawable.play)
            .setCustomBigContentView(notificationLayout)
            .setContentIntent(pendingIntent)
            .setAutoCancel(true)
            .build() //.builder() sets all the properties of the notifications, but .build() actually builds it with all previous settings
        val notificationManager = NotificationManagerCompat.from(this)

        mediaPlayer = MediaPlayer.create(this, R.raw.music)
        songTime = mediaPlayer.duration

        //I need to declare all variables that will use an ID from the xml file
        val playBtn: ImageButton = findViewById(R.id.play_btn)
        val songTitle: TextView = findViewById(R.id.TV_song_title)
//        val songImg: ImageView = findViewById(R.id.song_img)
//        val prevBtn: ImageButton = findViewById(R.id.prev_btn)
//        val nextBtn: ImageButton = findViewById(R.id.next_btn)
        val seekBar: SeekBar = findViewById(R.id.seekbar)
        val songInfo: Button = findViewById(R.id.song_info)
        val notificationPlayBtn: ImageButton? = findViewById(R.id.notification_button)


        //Play button.
        playBtn.setOnClickListener {
            if (mediaPlayer.isPlaying) {
                //If music is playing, this button should pause it and change the icon to play
                mediaPlayer.pause()
                playBtn.setImageResource(R.drawable.play)
            } else {
                //If music is not playing then it'll play and the button should change to pause
                mediaPlayer.start()
                playBtn.setImageResource(R.drawable.pause)
                notificationManager.notify(notificationId, notification)
            }
        }
        //Play button in notification
        notificationPlayBtn?.setOnClickListener{
            if (mediaPlayer.isPlaying) {
                mediaPlayer.pause()
                notificationPlayBtn.setImageResource(R.drawable.play)
            } else {
                mediaPlayer.start()
                notificationPlayBtn.setImageResource(R.drawable.pause)
            }
        }

        //SeekBar.
        seekBar.max = songTime
        seekBar.setOnSeekBarChangeListener(
            object : SeekBar.OnSeekBarChangeListener {
                override fun onProgressChanged(
                    seekBar: SeekBar?,
                    progress: Int,
                    change: Boolean
                ) {
                    if (change) {
                        mediaPlayer.seekTo(progress)
                    }
                }

                override fun onStartTrackingTouch(seekBar: SeekBar?) {

                }

                override fun onStopTrackingTouch(seekBar: SeekBar?) {
                }
            }
        )

        var flag = false
        runnable = Runnable {
            //I used a flag because even when setOnCompletionListener gave a "back to 0" order,
            //the seekBar would later go back to the end of the song
            if (flag) {
                seekBar.progress = 0
            } else {
                seekBar.progress = mediaPlayer.currentPosition
                handler.postDelayed(runnable, 1000)
            }


        }

        handler.postDelayed(runnable, 1000)

        mediaPlayer.setOnCompletionListener {
            seekBar.progress = 0
            playBtn.setImageResource(R.drawable.play)
            flag = true
        }

        //Song Info Button.
        songInfo.setOnClickListener {
            //For some reason the IDE forces me to use a different name for the second activity
            val intentInfo = Intent(this, InfoActivity1::class.java)
            startActivity(intentInfo)
        }

        val mediaPath: Uri = Uri.parse("android.resource://" + packageName + "/" + R.raw.music)
        val meta = MediaMetadataRetriever()
        meta.setDataSource(this, mediaPath)
        songTitle.text = meta.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE)


    }

    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_DEFAULT)

            val manager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            manager.createNotificationChannel(channel)
        }

    }

}